library dashboard;

import 'package:flutter/material.dart';
import 'package:lab_6/components/menu_items.dart';
import 'package:lab_6/components/page_header.dart';

const reportDummyData = [
  {
    "title": "Report: INI APLIKASI?",
    "subtitle": "Status: Open, Replies: 0",
  },
  {
    "title": "Report: Aplikasi Jelek, 1 star :(",
    "subtitle": "Status: Closed, Replies: 0",
  },
];

class Reports extends StatelessWidget {
  const Reports({Key? key}) : super(key: key);
  final title = "Dashboard";

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 0),
      child: ListView(
        // Column is also a layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        children: <Widget>[
          const Padding(
              child: PageHeader(
                title: "Have any problems using KonvaSearch?",
                subtitle:
                    "Create a report and get support from our reliable staff!",
              ),
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 12)),
          ...reportDummyData.map((item) {
            return Padding(
                child: MenuItem(
                    title: item["title"] ?? "",
                    subtitle: item["subtitle"] ?? "",
                    onClick: () {}),
                padding:
                    const EdgeInsets.symmetric(vertical: 2, horizontal: 14));
          }),
        ],
      ),
    ));
  }
}
