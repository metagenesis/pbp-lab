from django.db import models

# Create your models here.
class Note(models.Model):
  messageTo = models.CharField(max_length=128)
  messagefrom = models.CharField(max_length=128)
  title = models.CharField(max_length=64)
  message = models.CharField(max_length=64)
