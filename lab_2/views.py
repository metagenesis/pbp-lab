from django.http.response import HttpResponse
from django.shortcuts import render
from .models import Note

from django.core import serializers

# Create your views here.
def index(request):
  notes = Note.objects.all()
  response = {
    'notes': notes
  }
  return render(request, 'index_lab2.html', response)

def xml(request):
  notes = Note.objects.all()
  xmlNotes = serializers.serialize('xml', notes)
  return HttpResponse(xmlNotes, content_type="application/xml")

def json(request):
  notes = Note.objects.all()
  xmlNotes = serializers.serialize('json', notes)
  return HttpResponse(xmlNotes, content_type="application/json")