**Hasil dari page /lab-2:**

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Notes!</title>
  </head>
  <body>
    <table>
      <tr>
        <th>To</th>
        <th>From</th>
        <th>Title</th>
        <th>Message</th>
      </tr>

      <tr>
        <td>Myself</td>
        <td></td>
        <td>Hello</td>
        <td>Just saying hello uwu</td>
      </tr>

      <tr>
        <td>Myself</td>
        <td></td>
        <td>Don&#x27;t remember to sleep</td>
        <td>h3h3</td>
      </tr>

      <!-- END SECTION -->
    </table>
  </body>
</html>
```

**Hasil dari page /lab-2/json:**

```json
[
  {
    "model": "lab_2.note",
    "pk": 1,
    "fields": {
      "messageTo": "Myself",
      "messagefrom": "Myself",
      "title": "Hello",
      "message": "Just saying hello uwu"
    }
  },
  {
    "model": "lab_2.note",
    "pk": 2,
    "fields": {
      "messageTo": "Myself",
      "messagefrom": "Myself",
      "title": "Don't remember to sleep",
      "message": "h3h3"
    }
  }
]
```

**Hasil dari page /lab-2/xml:**

```xml
<django-objects version="1.0">
  <object model="lab_2.note" pk="1">
    <field name="messageTo" type="CharField">Myself</field>
    <field name="messagefrom" type="CharField">Myself</field>
    <field name="title" type="CharField">Hello</field>
    <field name="message" type="CharField">Just saying hello uwu</field>
  </object>
  <object model="lab_2.note" pk="2">
    <field name="messageTo" type="CharField">Myself</field>
    <field name="messagefrom" type="CharField">Myself</field>
    <field name="title" type="CharField">Don't forget to sleep</field>
    <field name="message" type="CharField">h3h3</field>
  </object>
</django-objects>
```

# Jawaban Pertanyaan Lab 2

**1. Apakah perbedaan antara JSON dan XML?**

**Jawab:** JSON (JavaScript Object Notation) memiliki perbedaan dari XML pada struktur key-value pair dari data yang di-serialize. Notasi JSON lebih mirip dengan syntax dictionary pada Python atau Object pada JavaScript. Sementara itu, XML atau Extensible Markup Language memiliki struktur yang lebih mirip dengan HTML, dimana setiap "block" data didefinisikan oleh sebuah tag yang identik dengan tag pada HTML.

**2. Apakah perbedaan antara HTML dan XML?**

**Jawab:** Kedua bahasa tersebut memiliki syntax yang mirip karena merupakan markup language, namun mereka memiliki fungsi yang beda.

HTML digunakan sebagai bahasa markup standar untuk membuat aplikasi web, sementara itu XML merupakan markup language yang bersifat lebih umum untuk _encoding_ data dalam format yang dapat dibaca oleh mesin dan manusia.

Selain itu, HTML memiliki tipe tag yang sudah terdefinisi, sementara untuk XML penamaan dari tag bersifat arbitrer dan bergantung pada programmer yang ingin menggunakan XML tersebut untuk mengirim data.
