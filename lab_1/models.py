from django.db import models

# Create Friend model with requested fields
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.BigIntegerField()
    DOB = models.DateField()
    def __str__(self):
        return f'{self.name} - {self.npm}'
