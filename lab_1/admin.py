from django.contrib import admin
from .models import Friend

# Register Friend model to the admin site.
admin.site.register(Friend)