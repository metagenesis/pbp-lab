from django.urls import path
from .views import index, add_friend, signout, delete_friend

urlpatterns = [
  path('', index),
  path('add', add_friend),
  path('sign-out', signout),
  path('delete', delete_friend)
]
