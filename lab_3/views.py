from django.shortcuts import render
from lab_1.models import Friend
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import FriendForm

# Create your views here.
def index(request):
  friends = Friend.objects.all()
  response = {
    "friends": friends,
    "user": request.user,
  }
  return render(request, "index_lab3.html", response)   

@login_required(login_url="/admin/login/?next=/lab-3/add")
def add_friend(request):
  if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FriendForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-3')
        else:
          print("FAILED")
          return HttpResponseRedirect("/lab-3/add")
  return render(request, "form.html")

def delete_friend(request):
  if request.user.is_authenticated:
    friend = Friend.objects.get(id=request.GET.get('id'))
    friend.delete()
  return HttpResponseRedirect('/lab-3')

def signout(request):
  logout(request)
  return HttpResponseRedirect("/lab-3/")