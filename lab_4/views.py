from django.shortcuts import render
from lab_2.models import Note
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import NoteForm

# Create your views here.
def index(request):
  notes = Note.objects.all()
  response = {
    "notes": notes,
    "user": request.user,
  }
  return render(request, "lab4_index.html", response)   

def cards(request):
  notes = Note.objects.all()
  response = {
    "notes": notes,
    "user": request.user,
  }
  return render(request, "lab4_card.html", response)

@login_required(login_url="/admin/login/?next=/lab-3/add")
def add_note(request):
  if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-4')
        else:
          print("FAILED")
          return HttpResponseRedirect("/lab-4/add-note")
  return render(request, "lab4_form.html")

def remove_note(request):
  if request.user.is_authenticated:
    note = Note.objects.get(id=request.GET.get('id'))
    note.delete()
  return HttpResponseRedirect('/lab-4')

def signout(request):
  logout(request)
  return HttpResponseRedirect("/lab-4/")