from django.urls import path
from .views import index, add_note, remove_note, cards

urlpatterns = [
  path('', index),
  path('cards', cards),
  path('add-note', add_note),
  path('delete', remove_note)
]
