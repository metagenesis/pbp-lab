library pointer_cursor;

import 'package:flutter/material.dart';

class PointerCursor extends StatelessWidget {
  const PointerCursor({Key? key, required this.child}) : super(key: key);

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: child,
    );
  }
}
